//
//  DirectionsViewController.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/2/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class DirectionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var directionsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        directionsTableView.delegate = self
        directionsTableView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
  
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        if section == 0 {
            let corImage = UIImageView()
            corImage.image = UIImage(named: "compass")
            corImage.frame = CGRect(x: 10, y: 10, width: 70, height:70)
            view.addSubview(corImage)
            
            view.layer.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            let label = UILabel()
            label.frame = CGRect(x: 130, y: 30, width: 120, height: 30)
            label.text = "Cooordinations"
            label.textColor = UIColor.white
            view.addSubview(label)
        } else {
            let dirImage = UIImageView()
            dirImage.image = UIImage(named: "directions")
            dirImage.frame = CGRect(x: 10, y: 10, width: 70, height:70)
            view.addSubview(dirImage)
            
              view.layer.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.5019607843, blue: 0.6901960784, alpha: 1)
            let label = UILabel()
            label.frame = CGRect(x: 130, y: 30, width: 120, height: 30)
            label.text = "Directions"
            label.textColor = UIColor.white
            view.addSubview(label)
        }
       
        return view
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 4
        } else {
            return 7
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = directionsTableView.dequeueReusableCell(withIdentifier: "DirectionsTableViewCell", for: indexPath) as? DirectionsTableViewCell
            else {
                return UITableViewCell()
        }
//        let ids = indexPath.section
//        let idx = indexPath.row
        cell.directionImage.image = UIImage(named: "compass")
        
        return cell
    }
    
    @IBAction func directionAudioPressed(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
