//
//  TimeViewController.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/18/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class TimeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var timeTableView: UITableView!
    let arrayFruits = ["Banana", "Apple", "Orange", "Pineapple", "Pear"]
    let ArrayCars = ["VW", "Porsche", "Audi", "Mercedes", "Honda", "Toyota", "Chevy"]
    let arrayKitchen = ["Stove", "Oven", "refrigerator", "Spoon", "Fork", "Plate", "Cup", "Pan"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeTableView.delegate = self
        timeTableView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch (section) {
            case 0:
                return arrayFruits.count
            case 1:
                return ArrayCars.count
            default:
            return arrayKitchen.count
    }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        switch section {
//        case 0:
//            return "Days "
//        case 1:
//            return "Months"
//        default:
//            return "Years"
//        }
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        switch section {
        case 0:
            let view1 = UIView()
            let label1 = UILabel()
            let button = UIButton()
            button.setTitle("click Me", for: .normal)
            button.backgroundColor = UIColor.blue
            label1.text = "Hello There"
            label1.textColor = UIColor.black
            label1.font = UIFont.boldSystemFont(ofSize: 16)
            
            button.translatesAutoresizingMaskIntoConstraints = false
            label1.translatesAutoresizingMaskIntoConstraints = false
    
            
            view1.addSubview(label1)
            //view1.addSubview(button)
            
            return view1
            
        case 1:
            let view2 = UIView()
            let label2 = UILabel()
            label2.text = "Section 2"
            view.addSubview(label2)
            return view2
        default:
            let view3 = UIView()
            let label3 = UILabel()
            label3.text = "Section 3"
            view.addSubview(label3)
            return view3
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
        case 0:
            let cell = timeTableView.dequeueReusableCell(withIdentifier: "DayTableViewCell", for: indexPath) as! DayTableViewCell
            
            cell.dayLabel.text = arrayFruits[indexPath.row]
            cell.contentView.backgroundColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
            return cell
        case 1 :
            let cell = timeTableView.dequeueReusableCell(withIdentifier: "MonthTableViewCell", for: indexPath) as! MonthTableViewCell
            cell.monthLabel.text = ArrayCars[indexPath.row]
            cell.contentView.backgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
            return cell
        default:
            let cell = timeTableView.dequeueReusableCell(withIdentifier: "YearTableViewCell", for: indexPath) as! YearTableViewCell
            cell.yearLabel.text = arrayKitchen[indexPath.row]
            cell.contentView.backgroundColor = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
            return cell
        }
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
