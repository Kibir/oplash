//
//  AlphabetsViewController.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/1/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit
import AVFoundation

class AlphabetsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var player: AVAudioPlayer?
    
    @IBOutlet weak var AlphabetsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AlphabetsTableView.delegate = self
        AlphabetsTableView.dataSource = self
        
        AlphabetsFunctions.getAllAlphabets(completion: {[weak self] in
            self?.AlphabetsTableView.reloadData()
        })
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.alphabetsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlphabetsTableViewCell", for: indexPath) as? AlphabetsTableViewCell
        else {
            return UITableViewCell()
        }
        cell.audioLetter.tag = indexPath.row
        cell.letter?.text = Data.alphabetsModel[indexPath.row].letter
        cell.tLetter?.text = Data.alphabetsModel[indexPath.row].tLetter
        cell.letterPrononciation?.text = Data.alphabetsModel[indexPath.row].letterPrononciation
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }

    
    @IBAction func alphabetAudioButtonTapped(_ sender: UIButton) {
        guard let path = Bundle.main.url(forResource: "alphabet\(sender.tag)", withExtension: "wav") else { return }
        print(sender.tag)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .duckOthers)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: path, fileTypeHint: AVFileType.wav.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            let button = sender
            guard let player = player else { return }
            player.play()
            button.Flash()
            
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
}
