//
//  MonthsViewController.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/11/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class MonthsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
 
    

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonthsCollectionViewCell", for: indexPath) as! MonthsCollectionViewCell
        cell.contentView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.contentView.layer.borderWidth = 1
        cell.contentView.layer.cornerRadius = 12
        cell.tMonth.text? = "Tamazight"
        cell.monthImage.image = UIImage(named: "january")
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
