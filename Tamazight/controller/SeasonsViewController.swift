//
//  SeasonsViewController.swift
//  Tamazight
//
//  Created by kabir Merakeb on 12/1/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class SeasonsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!

    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        SeasonFunction.getAllSeasons(completion: { [weak self] in
            self?.tableView.reloadData()
            
        })
        
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.seasonsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SeasonsTableViewCell", for: indexPath) as? SeasonsTableViewCell else {
            return UITableViewCell()
        }
        // styling the cell
        cell.contentView.layer.cornerRadius = 15
        cell.contentView.layer.borderColor = Themes.Accent?.cgColor
        cell.contentView.layer.borderWidth = 1
        
        let seasonIndex = indexPath.row
        cell.seasonLabel.text? = Data.seasonsModel[seasonIndex].season
        cell.seasonLatinLabel.text? = Data.seasonsModel[seasonIndex].seasonLatin
        cell.seasonTamazightLabel.text? = Data.seasonsModel[seasonIndex].seasonTamazight
        cell.seasonImageView.image? = UIImage(imageLiteralResourceName: Data.seasonsModel[seasonIndex].seasonImage)
        return cell
    }


}
