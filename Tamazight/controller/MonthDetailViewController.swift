//
//  MonthDetailViewController.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/11/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class MonthDetailViewController: UIViewController {

    @IBOutlet weak var monthView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        monthView.layer.cornerRadius = 12
        // Do any additional setup after loading the view.
    }
    

    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
