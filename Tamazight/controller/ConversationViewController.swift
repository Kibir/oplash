//
//  ConversationViewController.swift
//  Tamazight
//
//  Created by kabir Merakeb on 1/24/19.
//  Copyright © 2019 merakeb. All rights reserved.
//

import UIKit

class ConversationViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var conversationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        conversationTableView.dataSource = self
        conversationTableView.delegate = self
        conversationTableView.reloadData()
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationTableViewCell", for: indexPath) as! ConversationTableViewCell
        cell.conversationLatin.text? = "This is our first conversation, isn't it? This is our first conversation, isn't it? This is our first conversation, isn't it?This is our first conversation, isn't it?"
        cell.conversationTifinagh.text? = "ⴰⵏⴻⴱⴷⵓ ⴰⵏⴻⴱⴷⵓ ⴰⵏⴻⴱⴷⵓ ⴰⵏⴻⴱⴷⵓ ⴰⵏⴻⴱⴷⵓ ⴰⵏⴻⴱⴷⵓ"
        return cell
    }
}

