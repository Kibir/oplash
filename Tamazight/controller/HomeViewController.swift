//
//  HomeViewController.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/24/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit
import YouTubePlayer


class HomeViewController: UIViewController {
    @IBOutlet weak var featuredYoutubeVideo: YouTubePlayerView!
    
    @IBOutlet weak var storiesCollectionView: UICollectionView!
    @IBOutlet weak var wordTamazightLabel: UILabel!
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var wordLatinLabel: UILabel!
    @IBOutlet weak var wordDescriptionTextView: UITextView!
    
    var dailyWord = [DailyWord]()

    @IBOutlet weak var wordImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let myVideoURL = NSURL(string: "https://www.youtube.com/watch?v=4D8sSXUL5po")
        featuredYoutubeVideo.loadVideoURL(myVideoURL! as URL)
        
        storiesCollectionView.dataSource = self
        storiesCollectionView.delegate = self
        StoryFunctions.getAllStories(completion: { [weak self] in
            self?.storiesCollectionView.reloadData()
            })
        // get currency from firebase
        TamazightService.instance.getWord { (returnedWord) in
            self.dailyWord = returnedWord
            
            for word in self.dailyWord {
                
                // downloading the image from firebase
                
                let url: String = (URL(string: word.imageUrl!)?.absoluteString)!
                URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { (data, response, error) in
                    if error != nil {
                        print("Error: \(String(describing:error))")
                        return
                    }
                    DispatchQueue.main.async(execute: {
                        let image = UIImage(data: data!)
                        self.wordImageView.image = image
                        self.wordDescriptionTextView.text? = word.wordDescription
                        self.wordLabel.text? = word.word
                        self.wordTamazightLabel.text? = word.wordTamazight
                        self.wordLatinLabel.text? = word.wordLatin
                        
                    })
                }).resume()
                
            }
        }
        // Do any additional setup after loading the view.
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Data.storyModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = storiesCollectionView.dequeueReusableCell(withReuseIdentifier: "StoriesCollectionViewCell", for: indexPath) as! StoriesCollectionViewCell
        let idx = indexPath.row
        cell.storyTitle.text = Data.storyModel[idx].storyTitle
        //cell.storyPicture.image = Data.storyModel[idx].storyImage
        
        return cell
    }
    
    
}
