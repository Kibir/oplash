//
//  NumbersViewController.swift
//  Tamazight
//
//  Created by apple on 10/27/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit
import AVFoundation

class NumbersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var player: AVAudioPlayer?
    
    @IBOutlet weak var numbersTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numbersTableView.delegate = self
        numbersTableView.dataSource = self
        numbersTableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        NumbersFunctions.getAllNumbers(completion: { [weak self] in
            // completion will be called here
            self?.numbersTableView.reloadData()
        })

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.numbersModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = numbersTableView.dequeueReusableCell(withIdentifier: "NumbersTableViewCell", for: indexPath) as? NumbersTableViewCell
            else {
                return UITableViewCell()
            }
        cell.audioNumber.tag = indexPath.row
        cell.number?.text = Data.numbersModel[indexPath.row].number
        cell.tNumber?.text = Data.numbersModel[indexPath.row].tNumber
        cell.eNumber?.text = Data.numbersModel[indexPath.row].eNumber
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.0
//        UIView.animate(withDuration: 0.7) {
//            cell.alpha = 1.0
//        }
//        cell.layer.transform = CATransform3DTranslate(CATransform3DIdentity, -200, 10, 0)
//        // apply some animations to the cell
//        UIView.animate(withDuration: 0.5) {
//            cell.layer.transform = CATransform3DIdentity
//
//        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func numberAudioButtonTapped(_ sender: UIButton) {
        
        guard let path = Bundle.main.url(forResource: "audioNumber\(sender.tag)", withExtension: "wav") else { return }
        
        print("Clicked \(sender.tag)")
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: .default, options: .duckOthers)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: path, fileTypeHint: AVFileType.wav.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            sender.Pulse()
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
//        let button = sender
//
//        if button.isSelected == true {
//            button.backgroundColor = UIColor.green
//            button.setTitleColor(UIColor.white, for: .normal)
//            button.isSelected = false
//        } else {
//            button.backgroundColor = UIColor.red//Choose your colour here
//            button.setTitleColor(UIColor.white, for: .normal) //To change button Title colour .. check your button Tint color is clear_color..
//            button.isSelected = true
//        }
        
    }
    
}

