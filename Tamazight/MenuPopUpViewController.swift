//
//  MenuPopUpViewController.swift
//  Tamazight
//
//  Created by apple on 10/19/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class MenuPopUpViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var menuView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        menuView.layer.cornerRadius = 12
        // Do any additional setup after loading the view.
        profileImage.layer.cornerRadius = 35
    }
    @IBAction func SideMenuButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
