//
//  RoundedCornerView.swift
//  Tamazight
//
//  Created by apple on 10/18/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class RoundedCornerView: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
 
    override func draw(_ rect: CGRect) {
            layer.cornerRadius = 12
            layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            layer.borderWidth = 1
            layer.backgroundColor = #colorLiteral(red: 0.4246708751, green: 0.756203115, blue: 0.935259521, alpha: 1)
            
           // layer.shadowOpacity = 1
            //layer.shadowRadius = 2
            layer.shadowOffset = CGSize(width: 0, height: 7)
    }

}
