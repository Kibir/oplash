//
//  RoundedButton.swift
//  Tamazight
//
//  Created by apple on 10/17/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
  */
    override func draw(_ rect: CGRect) {
//        layer.borderWidth = 1
//        layer.borderColor = UIColor.gray.cgColor
        layer.cornerRadius = frame.height / 2
        //layer.cornerRadius = frame.width / 2
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 7
        layer.shadowOffset = CGSize(width: 0, height: 12)
        
    }
 

}
