//
//  UIButtonAnim.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/2/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func Pulse() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.12
        pulse.fromValue = 0.70
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: nil)
        
    }
    func Flash() {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.12
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        layer.add(flash, forKey: nil)
    }
    
    func Shake() {
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.12
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 7, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 7, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        layer.add(shake, forKey: nil)
    }
    
}
