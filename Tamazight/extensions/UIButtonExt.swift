//
//  UIButtonExt.swift
//  Tamazight
//
//  Created by apple on 10/17/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit
@IBDesignable

class UIButtonEx: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0 {
    didSet {
        layer.cornerRadius = cornerRadius
    }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = UIColor.clear.cgColor
        }
    }
    
}
