//
//  UIImageMaskView.swift
//  Tamazight
//
//  Created by apple on 10/20/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

@IBDesignable
class UIImageMaskView: UIImageView {
    
    var imageViewMask = UIImageView()
    
    @IBInspectable var imageMask: UIImage? {
    didSet {
        imageViewMask.image = imageMask
        imageViewMask.frame = bounds
        mask = imageViewMask
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
