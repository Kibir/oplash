//
//  NumbersTableViewCell.swift
//  Tamazight
//
//  Created by apple on 10/27/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class NumbersTableViewCell: UITableViewCell {

    @IBOutlet weak var eNumber: UILabel!
    @IBOutlet weak var audioNumber: UIButton!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var tNumber: UILabel!
    @IBOutlet weak var number: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        numberView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        numberView.layer.cornerRadius = 12
        numberView.layer.borderWidth = 2
        numberView.layer.borderColor = Themes.Accent?.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
