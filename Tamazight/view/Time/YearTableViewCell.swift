//
//  YearTableViewCell.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/18/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class YearTableViewCell: UITableViewCell {

    @IBOutlet weak var yearLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
