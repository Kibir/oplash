//
//  DirectionsTableViewCell.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/2/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class DirectionsTableViewCell: UITableViewCell {
    @IBOutlet weak var direction: UILabel!
    @IBOutlet weak var directionImage: UIImageView!
    @IBOutlet weak var directionSpelling: UILabel!
    
    @IBOutlet weak var tDirection: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
