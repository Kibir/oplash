//
//  ConversationTableViewCell.swift
//  Tamazight
//
//  Created by kabir Merakeb on 1/24/19.
//  Copyright © 2019 merakeb. All rights reserved.
//

import UIKit

class ConversationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ConversationImage: UIImageView!
    
    @IBOutlet weak var conversationLatin: UILabel!
    @IBOutlet weak var conversationTifinagh: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        conversationLatin.numberOfLines = 0
        conversationLatin.backgroundColor = . yellow
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
