//
//  MonthsCollectionViewCell.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/11/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class MonthsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var monthView: UIView!
    
    @IBOutlet weak var monthPrononciation: UILabel!

    @IBOutlet weak var tMonth: UILabel!
    
    @IBOutlet weak var monthImage: UIImageView!
    
}
