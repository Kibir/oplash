//
//  StoriesCollectionViewCell.swift
//  Tamazight
//
//  Created by kabir Merakeb on 1/16/19.
//  Copyright © 2019 merakeb. All rights reserved.
//

import UIKit

class StoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var storyPicture: UIImageView!
    
    @IBOutlet weak var storyTitle: UILabel!
}
