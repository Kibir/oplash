//
//  SeasonsTableViewCell.swift
//  Tamazight
//
//  Created by kabir Merakeb on 12/1/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class SeasonsTableViewCell: UITableViewCell {
    @IBOutlet weak var seasonLabel: UILabel!
    @IBOutlet weak var seasonLatinLabel: UILabel!
    @IBOutlet weak var seasonTamazightLabel: UILabel!
    @IBOutlet weak var seasonImageView: UIImageView!
    @IBOutlet weak var seasonAudioButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
