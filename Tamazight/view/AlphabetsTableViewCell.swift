//
//  AlphabetsTableViewCell.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/1/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class AlphabetsTableViewCell: UITableViewCell {

    @IBOutlet weak var alphabetView: UIView!
    @IBOutlet weak var letter: UILabel!
    @IBOutlet weak var tLetter: UILabel!
    @IBOutlet weak var letterPrononciation: UILabel!
    @IBOutlet weak var audioLetter: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        alphabetView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        alphabetView.layer.cornerRadius = 12
        alphabetView.layer.borderWidth = 2
        alphabetView.layer.borderColor = Themes.Accent?.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
