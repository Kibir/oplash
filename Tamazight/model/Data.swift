//
//  Data.swift
//  Tamazight
//
//  Created by apple on 10/31/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import Foundation

class Data {
    static var numbersModel = [NumbersModel]()
    static var alphabetsModel = [AlphabetsModel]()
    static var seasonsModel = [SeasonsModel]()
    static var storyModel = [StoryModel]()
}
