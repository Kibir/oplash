//
//  StoryFunctions.swift
//  Tamazight
//
//  Created by kabir Merakeb on 1/17/19.
//  Copyright © 2019 merakeb. All rights reserved.
//

import Foundation

class StoryFunctions {
    
    static func getAllStories(completion: @escaping() -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            if Data.storyModel.count == 0 {
                Data.storyModel.append(StoryModel(storyTitle: "Au Marche", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                Data.storyModel.append(StoryModel(storyTitle: "A L'airoport", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                Data.storyModel.append(StoryModel(storyTitle: "A l'ecole", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                Data.storyModel.append(StoryModel(storyTitle: "A l'Hotel", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                Data.storyModel.append(StoryModel(storyTitle: "Chez le Dentist", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                Data.storyModel.append(StoryModel(storyTitle: "Chez le Doctor", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                Data.storyModel.append(StoryModel(storyTitle: "Demander les Infos", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                 Data.storyModel.append(StoryModel(storyTitle: "a La Cite", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                 Data.storyModel.append(StoryModel(storyTitle: "les Directions", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                 Data.storyModel.append(StoryModel(storyTitle: "Au Restaurant", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                 Data.storyModel.append(StoryModel(storyTitle: "Au Cinema", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
                 Data.storyModel.append(StoryModel(storyTitle: "A la plage", storyImage: "blue-circle", storyDescription: "This one is the first Story", storyThumbnail: "blue-circle", storyTifinagh: "ⴰⵎⵡⴰⵏ"))
            }
        }
        completion()
    }
}
