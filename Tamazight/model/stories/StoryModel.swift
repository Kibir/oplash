//
//  StoryModel.swift
//  Tamazight
//
//  Created by kabir Merakeb on 1/17/19.
//  Copyright © 2019 merakeb. All rights reserved.
//

import Foundation

class StoryModel {
    var storyTitle: String
    var storyImage: String
    var storyDescription: String
    var storyThumbnail: String
    var storyTifinagh: String
    
    init(storyTitle: String, storyImage: String, storyDescription: String, storyThumbnail:String, storyTifinagh: String) {
        self.storyTitle = storyTitle
        self.storyImage = storyImage
        self.storyDescription = storyDescription
        self.storyThumbnail = storyThumbnail
        self.storyTifinagh = storyTifinagh
    }
}
