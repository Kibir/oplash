//
//  AlphabetsModel.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/1/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import Foundation

class AlphabetsModel {
    var letter: String
    var tLetter: String
    var letterPrononciation: String
    var letterAudio: String
    
    init(letter: String, tLetter: String, letterPrononciation: String, letterAudio: String) {
        self.letter = letter
        self.tLetter = tLetter
        self.letterPrononciation = letterPrononciation
        self.letterAudio = letterAudio
    }
}
