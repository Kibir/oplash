//
//  AlphabetsFunctions.swift
//  Tamazight
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import Foundation

class AlphabetsFunctions {
    
    static func getAllAlphabets(completion: @escaping() -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            if Data.alphabetsModel.count == 0 {
    
                Data.alphabetsModel.append(AlphabetsModel(letter: "a", tLetter: "ⴰ", letterPrononciation: "ya", letterAudio: "alphabet0"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "b", tLetter: "ⴱ", letterPrononciation: "yab", letterAudio: "alphabet1"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "g", tLetter: "ⴳ", letterPrononciation: "yag", letterAudio: "alphabet2"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "gʷ", tLetter: "ⴳⵯ", letterPrononciation: "yagʷ", letterAudio: "alphabet3"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "d", tLetter: "ⴷ", letterPrononciation: "yad", letterAudio: "alphabet4"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "ḍ", tLetter: "ⴹ", letterPrononciation: "yaḍ", letterAudio: "alphabet5"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "e", tLetter: "ⴻ", letterPrononciation: "yey", letterAudio: "alphabet6"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "f", tLetter: "ⴼ", letterPrononciation: "yaf", letterAudio: "alphabet7"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "k", tLetter: "ⴽ", letterPrononciation: "yak", letterAudio: "alphabet8"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴽⵯ", letterPrononciation: "yakʷ", letterAudio: "alphabet9"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵀ", letterPrononciation: "yah", letterAudio: "alphabet10"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵃ", letterPrononciation: "yaḥ", letterAudio: "alphabet11"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵄ", letterPrononciation: "yaɛ", letterAudio: "alphabet12"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵅ", letterPrononciation: "yax", letterAudio: "alphabet13"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵇ", letterPrononciation: "yaq", letterAudio: "alphabet14"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵉ", letterPrononciation: "yi", letterAudio: "alphabet15"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵊ", letterPrononciation: "yaj", letterAudio: "alphabet16"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵍ", letterPrononciation: "yal", letterAudio: "alphabet17"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵎ", letterPrononciation: "yam", letterAudio: "alphabet18"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵏ", letterPrononciation: "yan", letterAudio: "alphabet19"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵓ", letterPrononciation: "yu", letterAudio: "alphabet20"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵔ", letterPrononciation: "yar", letterAudio: "alphabet21"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵕ", letterPrononciation: "yaṛ", letterAudio: "alphabet22"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵖ", letterPrononciation: "yagh", letterAudio: "alphabet23"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵙ", letterPrononciation: "yas", letterAudio: "alphabet24"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵚ", letterPrononciation: "yaṣ", letterAudio: "alphabet25"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵛ", letterPrononciation: "yac", letterAudio: "alphabet26"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵜ", letterPrononciation: "yat", letterAudio: "alphabet27"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵟ", letterPrononciation: "yaṭ", letterAudio: "alphabet28"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵢ", letterPrononciation: "yaw", letterAudio: "alphabet29"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵣ", letterPrononciation: "yay", letterAudio: "alphabet30"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵥ", letterPrononciation: "yaz", letterAudio: "alphabet31"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴲ", letterPrononciation: "yaẓ", letterAudio: "alphabet32"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴴ", letterPrononciation: "String", letterAudio: "alphabet33"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴵ", letterPrononciation: "String", letterAudio: "alphabet34"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴶ", letterPrononciation: "String", letterAudio: "alphabet35"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴸ", letterPrononciation: "String", letterAudio: "alphabet36"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴺ", letterPrononciation: "String", letterAudio: "alphabet37"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴾ", letterPrononciation: "String", letterAudio: "alphabet38"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴿ", letterPrononciation: "String", letterAudio: "alphabet39"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵁ", letterPrononciation: "String", letterAudio: "alphabet40"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵂ", letterPrononciation: "String", letterAudio: "alphabet41"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵆ", letterPrononciation: "String", letterAudio: "alphabet42"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵈ", letterPrononciation: "String", letterAudio: "alphabet43"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵋ", letterPrononciation: "String", letterAudio: "alphabet44"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵌ", letterPrononciation: "String", letterAudio: "alphabet45"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵐ", letterPrononciation: "String", letterAudio: "alphabet46"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵑ", letterPrononciation: "String", letterAudio: "alphabet47"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵒ", letterPrononciation: "String", letterAudio: "alphabet48"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵓ", letterPrononciation: "String", letterAudio: "alphabet49"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵗ", letterPrononciation: "String", letterAudio: "alphabet50"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵘ", letterPrononciation: "String", letterAudio: "alphabet51"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵝ", letterPrononciation: "String", letterAudio: "alphabet52"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵞ", letterPrononciation: "String", letterAudio: "alphabet53"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵤ", letterPrononciation: "String", letterAudio: "alphabet54"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵦ", letterPrononciation: "String", letterAudio: "alphabet55"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵧ", letterPrononciation: "String", letterAudio: "alphabet56"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵜⵙ", letterPrononciation: "String", letterAudio: "alphabet57"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴷⵣ", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ɣ", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ɣ", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "θ̱", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "t͡ʃ", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "z", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "e", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "o", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "t͡s", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "d͡z", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⵜⵛ", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "ⴷⵊ", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "t͡ʃ", letterPrononciation: "String", letterAudio: "audio"))
                Data.alphabetsModel.append(AlphabetsModel(letter: "---", tLetter: "d͡ʒ", letterPrononciation: "String", letterAudio: "audio"))
            }
        }
        DispatchQueue.main.async {
            completion()
        }
    
    }
}
