//
//  SeasonFunctions.swift
//  Tamazight
//
//  Created by kabir Merakeb on 12/1/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class SeasonFunction {
    
    static func getAllSeasons(completion: @escaping() -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            if Data.seasonsModel.count == 0 {
                Data.seasonsModel.append(SeasonsModel(season: "Summer", seasonTamazight: "ⴰⵏⴻⴱⴷⵓ", seasonLatin: "Anebdu", seasonAudio: "", seasonImage: "Summer"))
                Data.seasonsModel.append(SeasonsModel(season: "Autumn", seasonTamazight: "ⴰⵎⵡⴰⵏ", seasonLatin: "Amwan", seasonAudio: "", seasonImage: "autumn"))
                Data.seasonsModel.append(SeasonsModel(season: "Spring", seasonTamazight: "ⵜⴰⴼⵙⵓⵜ", seasonLatin: "Tafsut", seasonAudio: "", seasonImage: "Spring"))
                Data.seasonsModel.append(SeasonsModel(season: "Winter", seasonTamazight: "ⵜⴰⴳⵔⴻⵙⵜ", seasonLatin: "Tagrest", seasonAudio: "", seasonImage: "Winter"))
            }
        }
        completion()
    
    }
}
