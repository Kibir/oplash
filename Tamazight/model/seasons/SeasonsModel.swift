//
//  SeasonsModel.swift
//  Tamazight
//
//  Created by kabir Merakeb on 12/1/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import Foundation

class SeasonsModel {
    var season: String
    var seasonTamazight: String
    var seasonLatin: String
    var seasonAudio: String
    var seasonImage: String
    
    init(season: String, seasonTamazight: String, seasonLatin: String, seasonAudio: String, seasonImage: String) {
        self.season = season
        self.seasonLatin = seasonLatin
        self.seasonTamazight = seasonTamazight
        self.seasonAudio = seasonAudio
        self.seasonImage = seasonImage
    }
}
