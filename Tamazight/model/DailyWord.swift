//
//  DailyWord.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/29/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class DailyWord {
    
    var wordDescription: String
    var word: String!
    var wordLatin: String!
    var wordTamazight: String!
    var imageUrl: String!
    
    init(word: String, wordLatin: String, wordTamazight: String, wordDescription: String, imageUrl: String) {
        self.word = word
        self.wordLatin = wordLatin
        self.wordTamazight = wordTamazight
        self.wordDescription = wordDescription
        self.imageUrl = imageUrl
    }
    
}
