//
//  NumbersModel.swift
//  Tamazight
//
//  Created by apple on 10/31/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import Foundation

class NumbersModel {
    var number: String
    var eNumber: String
    var tNumber: String
    var numberAudio: String
    
    init(number: String, eNumber: String, tNumber: String, numberAudio: String) {
        self.number = number
        self.eNumber = eNumber
        self.tNumber = tNumber
        self.numberAudio = numberAudio
    }
}
