//
//  NumberFunctions.swift
//  Tamazight
//
//  Created by apple on 10/31/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import Foundation
class NumbersFunctions {
    // since i will be using this class, i create a static function, so i don't need to call the class when i want to use the functions // static keyword allows me to access the class without initiating the class name
    
    static func getAllNumbers(completion: @escaping () -> ()){
        DispatchQueue.global(qos: .userInteractive).async {
            if Data.numbersModel.count == 0 {
                Data.numbersModel.append(NumbersModel(number: "0", eNumber: "Zero", tNumber: "ⵉⵍⴻⵎ", numberAudio: "audioNumber0"))
                Data.numbersModel.append(NumbersModel(number: "1", eNumber: "One", tNumber: "ⵢⵉuⴻⵏ", numberAudio: "audioNumber1"))
                Data.numbersModel.append(NumbersModel(number: "2", eNumber: "Two", tNumber: "ⵙⵉⵏ", numberAudio: "audioNumber2"))
                Data.numbersModel.append((NumbersModel(number: "3", eNumber: "Three", tNumber: "ⴽⵔⴰⴷ", numberAudio: "audioNumber3")))
                Data.numbersModel.append(NumbersModel(number: "4", eNumber: "Four", tNumber: "ⴽⵓⵥ", numberAudio: "audioNumber4"))
                Data.numbersModel.append(NumbersModel(number: "5", eNumber: "Five", tNumber: "ⵙⴻⵎⵎⵓⵙ", numberAudio: "audioNumber5"))
                Data.numbersModel.append(NumbersModel(number: "6", eNumber: "Six", tNumber: "ⵙⴷⵉⵙ", numberAudio: "audioNumber6"))
                Data.numbersModel.append(NumbersModel(number: "7", eNumber: "Seven", tNumber: "ⵙⴰ", numberAudio: "audioNumber7"))
                Data.numbersModel.append(NumbersModel(number: "8", eNumber: "Eight", tNumber: "ⵜⴰⵎ", numberAudio: "audioNumber8"))
                Data.numbersModel.append(NumbersModel(number: "9", eNumber: "Nine", tNumber: "ⵜⵥⴰ", numberAudio: "audioNumber9"))
                Data.numbersModel.append(NumbersModel(number: "10", eNumber: "Ten", tNumber: "ⵎⵔⴰu", numberAudio: "audioNumber10"))
                Data.numbersModel.append(NumbersModel(number: "20", eNumber: "Twenty", tNumber: "ⴰⴳⵏⴰⵔ", numberAudio: "audioNumber11"))
            }
        }
            DispatchQueue.main.async {
                completion()
            }
        
    }
    static func createNumber(numberModel: NumbersModel){
        
    }
    static func updateNumbers(numberMOdel: NumbersModel){
        
    }
    static func deleteNumbers(numberModel: NumbersModel){
        
    }
}
