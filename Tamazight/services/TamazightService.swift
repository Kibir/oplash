//
//  TamazightService.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/25/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import Foundation
import UIKit
import Firebase

let DB_REF = Database.database().reference()

class TamazightService {
    
    static let instance = TamazightService()
    
    private var _ref_base = DB_REF
    private var _ref_daily_word = DB_REF.child("dailyWords")
    private var _ref_history = DB_REF.child("history")
    
    var REF_BASE : DatabaseReference {
        return _ref_base
    }
    
    var REF_DAILY_WORD: DatabaseReference {
        return _ref_daily_word
    }
    
    var REF_HISTORY: DatabaseReference {
        return _ref_history
    }
    
    func getWord(handler: @escaping (_ dailyWord: [DailyWord]) -> () ) {
        
        var dailyWordArray = [DailyWord]()
        
        REF_DAILY_WORD.observeSingleEvent(of: .value) { (dailySnapShot) in
            guard let dailySnapShot = dailySnapShot.children.allObjects as? [DataSnapshot] else { return }
            
            for dailyWord in dailySnapShot {
                let wordDescription = dailyWord.childSnapshot(forPath: "wordDescription").value as! String
                let word = dailyWord.childSnapshot(forPath: "word").value as! String
                let wordLatin = dailyWord.childSnapshot(forPath: "wordLatin").value as! String
                let wordTamazight = dailyWord.childSnapshot(forPath: "wordTamazight").value as! String
                let imageUrl = dailyWord.childSnapshot(forPath: "imageUrl").value as! String
                let dailyArray = DailyWord(word: word, wordLatin: wordLatin, wordTamazight: wordTamazight, wordDescription: wordDescription, imageUrl: imageUrl)
                dailyWordArray.append(dailyArray)
            }
            handler(dailyWordArray)
            
        }
        
        
    }
}
