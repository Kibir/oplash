//
//  Themes.swift
//  Tamazight
//
//  Created by kabir Merakeb on 11/27/18.
//  Copyright © 2018 merakeb. All rights reserved.
//

import UIKit

class Themes {
    static let main_Font = "SF-Pro-Text-Regular.otf"
    static let tamazight_Tifinaghe_Font = "Tamazight Tifinaghe.ttf"
    static let tamazight_Latin_Font = "Tamazight Latin.ttf"
    
    static let Accent = UIColor(named: "Accent")
    static let BackgroundColor = UIColor(named: "Background")
    static let Tint = UIColor(named: "Tint")
}
